import './assets/main.css'


import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// Import Framework7 Bundle
import Framework7 from 'framework7/lite-bundle';

// Import Framework7-Vue with helper to register all components
import Framework7Vue, { registerComponents } from 'framework7-vue/bundle';

// Init plugin
Framework7.use(Framework7Vue)

const app = createApp(App)

app.use(router)
app.mount('#app')
registerComponents(app);
app.provide('appName', 'Vue3');
