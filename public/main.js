$(function () {
  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];
  var students = {};
  var userId = 0;

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
    return false;
  };

  // Initialize variables
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box
  var chatId = getUrlParameter('chatId');
  var admin = getUrlParameter('admin');
  var companyId = getUrlParameter('companyId');

  var $loginPage = $('.login.page'); // The login page
  var $chatPage = $('.chat.page'); // The chatroom page

  $window.ready(function() {
    var url = new URL(window.location);
    var username = url.searchParams.get("username");
    userId = url.searchParams.get("userId");
    if(username){
      var $usernameInput = $('.usernameInput');
      $usernameInput.val(username);
      setUsername();
    }

    getBlockedCandidateApiCall();
  });

  // Prompt for setting a username
  var username;
  var connected = false;
  var typing = false;
  var lastTypingTime;
  var $currentInput = $usernameInput.focus();

  var socket = io();

  const addParticipantsMessage = (data) => {
    var message = '';
    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + data.numUsers + " participants";
    }
    log(message);
  }

  // Sets the client's username
  const setUsername = () => {
    username = cleanInput($usernameInput.val().trim());

    // If the username is valid
    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();
      $loginPage.off('click');
      $currentInput = $inputMessage.focus();

      if(username != "" && admin != 1  && companyId != 0)
        
      // Tell the server your username
      socket.emit('add user', username, chatId, userId);
    }
  }

  const getDateTime = () =>{
    let today = new Date();

    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    let dateTime = date + ' ' + time;
    return dateTime;
  }
  // Sends a chat message
  const sendMessage = () => {
    var message = $inputMessage.val();
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', {
        username,
        message,
        userId: 0,
        messageAt: getDateTime()
      });
      addChatMessage({
        username: username,
        message: message
      });
      
    }
  }

  const blockCandidate = (userId) => {
    socket.emit('blockCandidate', {userId})
  }

  const unblockCandidate = (userId) => {
    socket.emit('unblockCandidate', {userId})
  }
// Log a message
const log = (message, options) => {
  const logElement = document.createElement('li');
  logElement.classList.add('log');
  logElement.textContent = message;
  addMessageElement(logElement, options);
}
  

  // Adds the visual chat message to the message list
  const addChatMessage = (data, options) => {
    // Don't fade the message in if there is an 'X was typing'
    var $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }

    let $usernameDiv = $('<span class="username text-xl font-medium text-black"/>')
      .text(data.username)
      .css('color', getUsernameColor(data.username));
    let $messageBodyDiv = $('<p class="messageBody text-slate-500">')
      .text(data.message);
    let button = '';
    if(admin == 1){
      if(data.userId != undefined && data.userId != 0 && data.userId != null || true){
        button = $('<button class="blockbutton px-4 py-1 text-sm text-purple-600 font-semibold rounded-full border border-purple-200 hover:text-white hover:bg-purple-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2"> Block</button>').val(JSON.stringify(data))
      }
    }
    
    
    let typingClass = data.typing ? 'typing' : '';
    let $messageDiv = $('<li class="message"/>')
      .data('username', data.username)
      .addClass(typingClass)
      .append($usernameDiv, button , $messageBodyDiv);

    addMessageElement($messageDiv, options);
  }
  $(document).on('click','.blockbutton',function(){
    let blockmsg = JSON.parse(this.value);
    blockCandidate(blockmsg.userId);
    alert(blockmsg.username + " chat is blocked, he/she won't be able to send message again");
    getBlockedCandidateApiCall();
  })
  // Adds the visual chat typing message
  const addChatTyping = (data) => {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  const removeChatTyping = (data) => {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

const addMessageElement = (el, options) => {
  const $el = $(el);
  const defaultOptions = {
    fade: true,
    prepend: false
  };
  options = options || {};

  options = Object.assign({}, defaultOptions, options);

  if (options.fade) {
    $el.hide().fadeIn(FADE_TIME);
  }
  if (options.prepend) {
    $messages.prepend($el);
  } else {
    $messages.append($el);
  }
  $messages[0].scrollTop = $messages[0].scrollHeight;
}

const cleanInput = (input) => {
  const div = document.createElement('div');
  div.textContent = input;
  return div.innerHTML;
}

const updateTyping = () => {
  if (!connected) {
    return;
  }

  if (!typing) {
    typing = true;
    socket.emit('typing');
  }

  lastTypingTime = Date.now();

  setTimeout(() => {
    const typingTimer = Date.now();
    const timeDiff = typingTimer - lastTypingTime;

    if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
      socket.emit('stop typing');
      typing = false;
    }
  }, TYPING_TIMER_LENGTH);
};

// Returns the 'X is typing' messages of a user
const getTypingMessages = ({ username }) =>
  $('.typing.message').filter((i, element) => $(element).data('username') === username);

// Gets the color of a username through our hash function
const getUsernameColor = (username) => {
  // Compute hash code
  let hash = 7;
  for (let i = 0; i < username.length; i++) hash = username.charCodeAt(i) + (hash << 5) - hash;
  
  // Calculate color
  const index = Math.abs(hash % COLORS.length);
  
  return COLORS[index];
}

  // Keyboard events

  $window.keydown(event => {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (username) {
        sendMessage();
        socket.emit('stop typing');
        typing = false;
      } else {
        setUsername();
      }
    }
  });

  $inputMessage.on('input', () => {
    updateTyping();
  });

  // Click events

  // Focus input when clicking anywhere on login page
  $loginPage.click(() => {
    $currentInput.focus();
  });

  // Focus input when clicking on the message input's border
  $inputMessage.click(() => {
    $inputMessage.focus();
  });

  // Socket events

  // Whenever the server emits 'login', log the login message
  socket.on('login', (data) => {
    connected = true;
    // Display the welcome message
    var message = "Welcome to Coprep Chat – ";
    log(message, {
      prepend: true
    });
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', (data) => {
    console.log(data);
    data.forEach(function(asd){
      addChatMessage(asd.message);
    })
   
  });

  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', (data) => {
    log(data.username + ' joined');
    if(userId != 0){
      students[data.userId] = data.username;
    }
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', (data) => {
    log(data.username + ' left');
    if(userId != 0 && students[data.userId] != 0){
      delete students[data.userId];
    }
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', (data) => {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', (data) => {
    removeChatTyping(data);
  });

  socket.on('disconnect', () => {
    
    //log('you have been disconnected');
  });

  socket.on('reconnect', () => {
  //  log('you have been reconnected');
    if (username) {
      socket.emit('add user', username, chatId, userId);
    }
  });

  socket.on('reconnect_error', () => {
    log('attempt to reconnect has failed');
  });

  $(document).on('click','.unblockbutton',function(){
    let blockmsg = JSON.parse(this.value);
    unblockCandidate(blockmsg.userId);
    alert(blockmsg.username + " chat is unblocked, he/she can send message again");
    getBlockedCandidateApiCall();
  })



function getBlockedCandidateApiCall() {
  if (companyId > 0) {
    const body = {
      data: {
        companyId: companyId
      }
    };

    const form = new FormData();
    form.append("body", JSON.stringify(body));

    const options = {
      method: "POST",
      mimeType: "multipart/form-data",
      url: "https://backend.coprepedu.com/notification/chat/getChatBlockedCandidates",
      headers: {},
      processData: false,
      contentType: false,
      data: form
    };

    $.ajax(options).done(function (response) {
      response = JSON.parse(response);
      const candidateList = response.data.candidateList;
      console.log(candidateList);
      $("#blockedList").html("");
      candidateList.forEach(function (candidate) {
        const button = $("<button class='unblockbutton'> Unblock</button>").val(
          JSON.stringify({
            userId: candidate.candidateId,
            username: candidate.username
          })
        );
        $("#blockedList")
          .append("<li>")
          .append(
            "<span class='username'>" + candidate.username + " " + candidate.name + "</span>"
          )
          .append(button)
          .append("</li>");
      });
    });
  }

}



});
