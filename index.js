// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const redis = require("redis")
var _ = require('underscore');
var db;
var publisher;
var subscriber;

const RedisConfig = {
  host: "185.217.125.229",
  port: "6379",
  password: "alphaKennyone",
  detect_buffers: true
} 



const mongoose = require('mongoose');


const chatSchema = new mongoose.Schema({
  username: String, 
  message: {
    username: String,
    message: String,
    userId: String,
    messageAt: String
  },
  chatroom:String
});
chatSchema.index({chatroom:1})


var port = process.env.PORT || 3000;

server.listen(port, async () => {
  await mongoose.connect('mongodb://localhost/cprep_chat', { useNewUrlParser: true , useUnifiedTopology: true,useCreateIndex: true });
  db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));

  console.log('Server listening at port %d', port);

  publisher = redis.createClient(RedisConfig);
  subscriber = redis.createClient(RedisConfig);

  publisher.on("error", function(error) {
    console.error(error);
  });
  subscriber.on("error", function(error) {
    console.error(error);
  });
  

  subscriber.subscribe("new-message")
  subscriber.on("message", (channel,msg) => {
    data = JSON.parse(msg);
  
    data[0].sub = data[0].sub ?data[0].sub+1:0;
    console.log(data[0].chatroom); 
    socketm.broadcast.to(data[0].chatroom).emit('new message',data);
    var msg = new chatrooms(data[0]);
    msg.save();

  });

});

// Routing
app.use(express.static(path.join(__dirname, 'public')));
var usernames = {};
// Chatroom
var numUsers = [];
var chatrooms;
var socketm ;
var blockList = [];



io.on('connection', (socket) => {
  var addedUser = false;
  socketm = socket;
  
   socket.on('data', function(buf) {
       console.log(buf);
            var js = JSON.parse(buf);
            console.log(js);
            socket.emit(js.event,js.data); //Send the msg to socket.io clients
    });
  
  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    if(!_.contains(blockList,data.userId) && !data.published){
      data.published= true
      publisher.publish("new-message", JSON.stringify( [{
        username: socket.username,
        message: data,
        chatroom:socket.room
      }]), )
      
    }
    
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username, room, userId) => {
    if (addedUser) return;
    if (room == undefined) return;
    // we store the username in the socket session for this client
    socket.username = username;
    socket.userId = userId;
    usernames[username] = username;
    socket.room = room;
    if (numUsers[socket.room] == undefined) {
      numUsers[socket.room] = 0;
    }
    if (chatrooms == undefined) {
      chatrooms = mongoose.model("chat" , chatSchema);
    }
    chatrooms.find({"chatroom": room},function (err, previousMsgList) {
        if (err) { 
          console.error(err);
          return [];
        }
      socket.emit('new message', previousMsgList);
      
    });

    socket.join(socket.room);
    ++numUsers[socket.room];
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers[socket.room],
      userId: userId
    });

    // echo globally (all clients) that a person has connected
    socket.broadcast.to(socket.room).emit('user joined', {
      username: socket.username,
      userId: socket.userId,
      numUsers: numUsers[socket.room]
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    socket.broadcast.to(socket.room).emit('typing', {
      username: socket.username,
    });
  });


  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    socket.broadcast.to(socket.room).emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers[socket.room];

      // echo globally that this client has left
      socket.broadcast.to(socket.room).emit('user left', {
        username: socket.username,
        userId: socket.userId,
        numUsers: numUsers[socket.room]
      });
    }
  });

  
  // when client emits 'blockCandidate', we add it to block list and stop its msg from publishing
  socket.on('blockCandidate',(data) =>
  {
    if(!_.contains(blockList,data.userId)){
      blockList.push(data.userId);
      blockCandidateApiCall(data.userId);
    }
    console.log(blockList);
  });

  socket.on('unblockCandidate',(data) =>
  {
    if(_.contains(blockList,data.userId)){
      blockList = _.filter(blockList,function(num){ return num != data.userId ; },data);
    }
    unblockCandidateApiCall(data.userId);
    console.log(blockList);
  });


  async function blockCandidateApiCall(userId) {
    let body = {};
    body.data = {};
    body.data["candidateId"] = userId;
    body.data["blockChat"] = 1;
    var request = require('request');
    var options = {
      'method': 'POST',
      'url': 'https://backend.coprepedu.com/misc/Misc/blockCandidateChat',
      'headers': {
      },
      formData: {
      'body': JSON.stringify(body)
      }
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body);
    });

  }

  async function unblockCandidateApiCall(userId) {
    let body = {};
    body.data = {};
    body.data["candidateId"] = userId;
    body.data["blockChat"] = 0;
    console.log(body.data);
    var request = require('request');
    var options = {
      'method': 'POST',
      'url': 'https://backend.coprepedu.com/misc/Misc/blockCandidateChat',
      'headers': {
      },
      formData: {
      'body': JSON.stringify(body)
      }
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body);
    });

  }

  

});